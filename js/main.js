var Calendar = function (o) {
    this.divId = o.parentID;
    this.DaysOfWeek = o.DaysOfWeek;
    this.Months = o.Months;
    this.CurrentMonth = o.CurrentMonth;
    this.CurrentYear = o.CurrentYear;
    this.format = o.Format;
    this.clicked = o.clicked;
    this.firstDate = o.firstDate;
    this.secondDate = o.secondDate;
};

//Display next Month
Calendar.prototype.nextMonth = function () {
    if (this.CurrentMonth == 12) {
        this.CurrentMonth = 1;
        this.CurrentYear = this.CurrentYear + 1;
    } else {
        this.CurrentMonth = this.CurrentMonth + 1;
    }
    this.showCurrent();

};

//Display previous Month
Calendar.prototype.previousMonth = function () {
    if (this.CurrentMonth == 1) {
        this.CurrentMonth = 12;
        this.CurrentYear = this.CurrentYear - 1;
    } else {
        this.CurrentMonth = this.CurrentMonth - 1;
    }
    this.showCurrent();
};

//Display previous Year
Calendar.prototype.previousYear = function () {
    this.CurrentYear = this.CurrentYear - 1;
    this.showCurrent();
};

//Display next Year
Calendar.prototype.nextYear = function () {
    this.CurrentYear = this.CurrentYear + 1;
    this.showCurrent();
};

// Show current month in the calendar calling function showCalendar() with month and year attributes
Calendar.prototype.showCurrent = function () {
    this.showCalendar(this.CurrentMonth, this.CurrentYear);
    this.secondDate = '';
    this.firstDate = '';
    this.clicked = '';
};

//Show the Calendar creating an HTMl
Calendar.prototype.showCalendar = function (m, y) {

    //first day of the first week of the Current month, 1 indicates the first day
    var firstDayCurrentMonth = new Date(y, m - 1, 1).getDay();
    var onmouseover = 'onmouseover="changeBack(this)" onmouseout="normalBack(this)"';
    console.log(y);
    console.log(m);
    console.log(firstDayCurrentMonth);
    if (firstDayCurrentMonth == 0) {
        firstDayCurrentMonth = 7;
    }
    //Last Day of the Current Month, 0 indicates the last day
    var lastDateCurrentMonth = new Date(y, m, 0).getDate();
    console.log(lastDateCurrentMonth);
    //Day of the Previous Month, 0 indicates the last day
    //var lastDatePreviousMonth = (m == 0 ? (new Date(y - 1, 11, 0).getDate()) : (new Date(y, m, 0).getDate()));
    var monthandyearhtml = '<span id="monthandyearspan">' + this.Months[m - 1] + ' - ' + y + '</span>';
    var html = "<table style='margin: 5px'>";
    html += '<tr>';
    for (var i = 0; i < 7; i++) {
        html += '<th class="daysheader">' + this.DaysOfWeek[i] + '</th>';
    }
    html += '</tr>';
    var prev = 0;
    for (var i = 1, row = 0, wkend = 0; row < 6; row++) {
        html += '<tr>';
        for (var col = 0; col < 7; col++) {
            if (firstDayCurrentMonth > 1 && prev != firstDayCurrentMonth - 1) {
                html += '<td> </td>';
                prev++;
            }
            else if (i > lastDateCurrentMonth) {
                html += '<td> </td>';
            }
            else {
                if (wkend == 5 || wkend == 6 || wkend == 12 || wkend == 13 || wkend == 19 || wkend == 20 || wkend == 26 || wkend == 27 || wkend == 33 || wkend == 34) {

                    html += '<td  ' + onmouseover + 'style="color: slategray">' + (i) + '</td>';
                    i++;
                } else {
                    html += '<td ' + onmouseover + '>' + (i) + '</td>';
                    i++;
                }
            }
            wkend++;
        }
    }
    html += '</tr>';

    html += '</table>';

    getId("monthandyear").innerHTML = monthandyearhtml;
    getId(this.divId).innerHTML = html;
};

changeBack = function (x) {
    if (x.style.backgroundColor == 'rgb(26, 188, 156)') {
    }
    else {
        x.style.backgroundColor = 'white';
    }
};

normalBack = function (x) {
    if (x.style.backgroundColor == 'rgb(26, 188, 156)') {
    }
    else {
        x.style.backgroundColor = '#ddd';
    }
};

//onClick dates event handler
Calendar.prototype.onClickHandler = function (e) {
    e = e || window.event;
    var target = e.target || e.srcElement;
    if (target.tagName == ('TD') && target.innerHTML != 0) {
        if (target.style.backgroundColor == 'rgb(26, 188, 156)') {
            console.log((new Date(this.CurrentMonth + '-' + target.innerHTML + '-' + this.CurrentYear)).getTime() != this.firstDate.getTime());
            if (this.clicked == 1) {
                this.firstDate = '';
                target.style.backgroundColor = '#ddd';
                this.clicked--;
                getId("firstDate").setAttribute('value', '');
            }
            else if (this.clicked == 2 && (new Date(this.CurrentMonth + '-' + target.innerHTML + '-' + this.CurrentYear)).getTime() != this.firstDate.getTime()) {
                this.secondDate = '';
                getId("secondDate").setAttribute('value', '');
                target.style.backgroundColor = '#ddd';
                this.clicked--;
            }

        } else if (this.clicked < 2) {
            console.log(this.clicked);
            if (this.clicked == 0) {
                this.firstDate = new Date(this.CurrentMonth + '-' + target.innerHTML + '-' + this.CurrentYear);
                console.log(this.firstDate);
                this.clicked++;
                target.style.backgroundColor = '#1abc9c';
            }
            else {
                this.secondDate = new Date(this.CurrentMonth + '-' + target.innerHTML + '-' + this.CurrentYear);

                if (this.secondDate.getTime() <= this.firstDate) {
                    this.secondDate = '';
                }
                else {
                    target.style.backgroundColor = '#1abc9c';
                    this.clicked++;
                }
            }

        }
        console.log('first date :' + this.firstDate);
        console.log('second date :' + this.secondDate);
        if (this.firstDate != '') {
            getId("firstDate").setAttribute('value', (this.firstDate.getFullYear() + '-' + (this.firstDate.getMonth() < 10 ? '0' + (this.firstDate.getMonth() + 1) : (this.firstDate.getMonth() + 1)) + '-' + (this.firstDate.getDate() < 10 ? '0' + this.firstDate.getDate() : this.firstDate.getDate())  ));
            if (this.secondDate != '') {
                getId("secondDate").setAttribute('value', (this.secondDate.getFullYear() + '-' + (this.secondDate.getMonth() < 10 ? '0' + (this.secondDate.getMonth() + 1) : (this.secondDate.getMonth() + 1)) + '-' + (this.secondDate.getDate() < 10 ? '0' + this.secondDate.getDate() : this.secondDate.getDate())  ));
            }
        }
    }

};

Calendar.prototype.changeFormat1 = function () {
    if (this.firstDate != '') {
        getId("firstDate").setAttribute('value', new Date(this.firstDate).toString(getId('formatDateArrival').options[getId('formatDateArrival').selectedIndex].value));
    }
    else {
        getId("firstDate").setAttribute('value', '');
    }
};

Calendar.prototype.changeFormat2 = function () {
    console.log(this.secondDate);
    if (this.secondDate != '') {
        getId("secondDate").setAttribute('value', new Date(this.secondDate).toString(getId('formatDateDeparture').options[getId('formatDateDeparture').selectedIndex].value));
    } else {
        getId("secondDate").setAttribute('value', '');

    }

};

Calendar.prototype.daysBdates = function () {
    if (this.firstDate != '' && this.secondDate != '') {
        var days = Math.round(Math.abs((this.firstDate.getTime() - this.secondDate.getTime()) / (24 * 60 * 60 * 1000)));
        getId("daysBdates").setAttribute('value', days);
    } else {
        getId("daysBdates").setAttribute('value', '');

    }
};

//On load, initialize all the Calendar variables.
window.onload = function () {
    window.onclick = function () {
        cal.onClickHandler();
        cal.daysBdates();
        cal.changeFormat1();
        cal.changeFormat2();
    };

    var date = new Date();
    var cal = new Calendar({
        clicked: 0,
        parentID: "calendartable",
        //10, starting by 0
        CurrentMonth: date.getMonth() + 1,
        //116
        CurrentYear: date.getFullYear(),
        DaysOfWeek: [
            'MON',
            'TUE',
            'WED',
            'THU',
            'FRI',
            'SAT',
            'SUN'
        ],
        Months: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'],

    });
    cal.showCurrent();
    getId('btnPrev').onclick = function () {
        cal.previousMonth();
    };

    getId('btnPrevYr').onclick = function () {
        cal.previousYear();
    };

    getId('btnNext').onclick = function () {
        cal.nextMonth();
    };

    getId('btnNextYr').onclick = function () {
        cal.nextYear();
    };

    //Languages settings
    getId('english').style.border = "2px solid #ccc";
    getId('english').style.borderRadius = "6px";
    getId('english').onclick = function () {
        cal.changelanguage1();
        getId('english').style.border = "2px solid #ccc ";
        getId('english').style.borderRadius = "6px";
        getId('spanish').style.border = "0px";
        getId('spanish').style.borderRadius = "0px";
        getId('german').style.border = "0px";
        getId('german').style.borderRadius = "0px";
        getId('italian').style.border = "0px";
        getId('italian').style.borderRadius = "0px";
        getId('french').style.border = "0px";
        getId('french').style.borderRadius = "0px";
    };

    getId('spanish').onclick = function () {
        cal.changelanguage2();
        getId('spanish').style.border = "2px solid #ccc";
        getId('spanish').style.borderRadius = "6px";
        getId('english').style.border = "0px";
        getId('english').style.borderRadius = "0px";
        getId('german').style.border = "0px";
        getId('german').style.borderRadius = "0px";
        getId('italian').style.border = "0px";
        getId('italian').style.borderRadius = "0px";
        getId('french').style.border = "0px";
        getId('french').style.borderRadius = "0px";
    };

    getId('german').onclick = function () {
        cal.changelanguage3();
        getId('german').style.border = "2px solid #ccc";
        getId('german').style.borderRadius = "6px";
        getId('spanish').style.border = "0px";
        getId('spanish').style.borderRadius = "0px";
        getId('english').style.border = "0px";
        getId('english').style.borderRadius = "0px";
        getId('italian').style.border = "0px";
        getId('italian').style.borderRadius = "0px";
        getId('french').style.border = "0px";
        getId('french').style.borderRadius = "0px";
    };

    getId('french').onclick = function () {
        cal.changelanguage4();
        getId('french').style.border = "2px solid #ccc";
        getId('french').style.borderRadius = "6px";
        getId('spanish').style.border = "0px";
        getId('spanish').style.borderRadius = "0px";
        getId('german').style.border = "0px";
        getId('german').style.borderRadius = "0px";
        getId('italian').style.border = "0px";
        getId('italian').style.borderRadius = "0px";
        getId('english').style.border = "0px";
        getId('english').style.borderRadius = "0px";
    };

    getId('italian').onclick = function () {
        cal.changelanguage5();
        getId('italian').style.border = "2px solid #ccc";
        getId('italian').style.borderRadius = "6px";
        getId('spanish').style.border = "0px";
        getId('spanish').style.borderRadius = "0px";
        getId('german').style.border = "0px";
        getId('german').style.borderRadius = "0px";
        getId('english').style.border = "0px";
        getId('english').style.borderRadius = "0px";
        getId('french').style.border = "0px";
        getId('french').style.borderRadius = "0px";
    };
};

// Get element by id
function getId(id) {
    return document.getElementById(id);
};

//-------------------------------------------------Languages----------------------------------------------------
Calendar.prototype.changelanguage1 = function () {
    getId("lan1").innerHTML = 'Days between dates:';
    getId("lan2").innerHTML = 'Arrival Date:';
    getId("lan3").innerHTML = "Date's format";
    getId("lan4").innerHTML = 'Departure Date:';
    getId("lan5").innerHTML = "Date's format";

    this.DaysOfWeek = [
        'MON ',
        'TUE ',
        'WED ',
        'THU ',
        'FRI ',
        'SAT ',
        'SUN '
    ];

    this.Months = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'];
    this.showCurrent();
};

Calendar.prototype.changelanguage2 = function () {
    getId("lan1").innerHTML = 'Días entre fechas:';
    getId("lan2").innerHTML = 'Día de llegada:';
    getId("lan3").innerHTML = "Formato de la fecha";
    getId("lan4").innerHTML = 'Día de salida:';
    getId("lan5").innerHTML = "Formato de la fecha";

    this.DaysOfWeek = [
        'LUN ',
        'MAR ',
        'MIE ',
        'JUE ',
        'VIE ',
        'SAB ',
        'DOM'
    ];

    this.Months = [
        'Ene',
        'Feb',
        'Mar',
        'Abr',
        'May',
        'Jun',
        'Jul',
        'Ago',
        'Sep',
        'Oct',
        'Nov',
        'Dic'];
    this.showCurrent();
};

Calendar.prototype.changelanguage3 = function () {
    getId("lan1").innerHTML = 'Tage zwischen Terminen:';
    getId("lan2").innerHTML = 'Tag der Ankunft:';
    getId("lan3").innerHTML = "Datumsformat";
    getId("lan4").innerHTML = 'Abreisetag:';
    getId("lan5").innerHTML = "Datumsformat";

    this.DaysOfWeek = [
        'MON ',
        'DIE ',
        'MIT ',
        'DON ',
        'FRE ',
        'SAM ',
        'SON'
    ];

    this.Months = [
        'Jan',
        'Feb',
        'Mär',
        'Apr',
        'Mai',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Okt',
        'Nov',
        'Dez'];
    this.showCurrent();
};

Calendar.prototype.changelanguage4 = function () {
    getId("lan1").innerHTML = 'Jours entre deux dates:';
    getId("lan2").innerHTML = "Jour de l'arrivée:";
    getId("lan3").innerHTML = "Format de date";
    getId("lan4").innerHTML = 'Jour de départ:';
    getId("lan5").innerHTML = "Format de date";

    this.DaysOfWeek = [
        'LUN ',
        'MAR ',
        'MER ',
        'JEU ',
        'VEN ',
        'SAM ',
        'DIM'
    ];

    this.Months = [
        'Jan',
        'Fév',
        'Mar',
        'Avr',
        'Mai',
        'Juin',
        'Juillet',
        'Aoû',
        'Sep',
        'Oct',
        'Nov',
        'Déc'];
    this.showCurrent();
};

Calendar.prototype.changelanguage5 = function () {
    getId("lan1").innerHTML = 'Giorni tra le date:';
    getId("lan2").innerHTML = 'Giorno di arrivo:';
    getId("lan3").innerHTML = "Formato data";
    getId("lan4").innerHTML = 'Giorno di partenza:';
    getId("lan5").innerHTML = "Formato data";

    this.DaysOfWeek = [
        'LUN ',
        'MAR ',
        'MER ',
        'GIO ',
        'VEN ',
        'SAB ',
        'DOM'
    ];

    this.Months = [
        'gEN',
        'Feb',
        'Mar',
        'Apr',
        'Mag',
        'Giu',
        'Liu',
        'Ago',
        'Set',
        'Ott',
        'Nov',
        'Dic'];
    this.showCurrent();
};

